// DroidScript
// needs UIExtras plugin to run without modification
// 
// tries to handle screen rotation helpfully
// by placing controls vertically or horizontally
// as appropriate. This does not rely on the plugin

// the dateDiff function does not rely on the plugin
app.LoadPlugin( "UIExtras" );
var date1,date2;
var picker,picker2,lay,txt,txt2,mth={};;

function OnStart()
{ 
 setMth(mth);
 var orient = app.GetOrientation()=="Landscape"?"Horizontal":"Verical";
 lay = app.CreateLayout( "Linear", orient+",FillXY" );
 var lay1 = app.CreateLayout( "Linear", "" );
 var lay2 = app.CreateLayout( "Linear", "" );
 lay.AddChild( lay1 );
 lay.AddChild( lay2 );
 if(app.GetOrientation=="Landscape")
     lay.SetOrientation( "Horizontal" ); 
 var uix = app.CreateUIExtras();
 
 picker = uix.CreateDatePicker();
 picker.SetOnDateChanged( OnDateChanged );
 lay1.AddChild( picker );
 
 txt= app.CreateText( "",-1,-1,"autoscale" );
 txt.SetTextSize( 32 );
 txt.SetOnTouchUp( txt_OnTouch );
 lay1.AddChild( txt );

 picker2 = uix.CreateDatePicker();
 picker2.SetOnDateChanged( OnDate2Changed );
 lay2.AddChild( picker2 );
//btnToday = app.CreateButton("today")
//btnToday.SetOnTouch(tdot);
//lay2.AddChild(btnToday); 
txt2= app.CreateText( "",-1,-1,"autoscale" );
 txt2.SetTextSize( 32 );
// lay2.AddChild( txt2 );
 
 app.AddLayout( lay );
 init()
}

function tdot ()
{ 
 var date= new Date()
 var datee = new Date(date.getFullYear(),date.getMonth(),date.getDate());
picker2.SetDate(datee)
picker2.SetText(datee)
 

}

function txt_OnTouch()
{
	if(confirm("Reset dates?")) init();
}

function init()
{
 var date = new Date();
 date1 = new Date(date.getFullYear(),date.getMonth(),date.getDate());
 date2 = new Date(date.getFullYear(),date.getMonth(),(date.getDate()+0));
 //date2 = new Date(2019, mth.jan, 1);
 picker.SetDate(date1);
 picker2.SetDate(date2);
 showDiff();
}

function OnDateChanged( year, month, day )
{
 date1 = new Date( year, month, day);
 showDiff();
}
function OnDate2Changed( year, month, day )
{
 date2 = new Date( year, month, day);
 showDiff();
}

// something changed, update text.
function showDiff()
{
  var d = 1000*3600*24;
	var diff = Math.round((date2.getTime() - date1.getTime())/d);
	txt.SetText( diff + " days" );
	txt.SetTextSize( 32 ); // may have been undone by autoscale

	var diffObj = dateDiff(date1,date2);
	var s = diffObj.sign;
	if(diffObj.years > 0) s += diffObj.years+" years, "
	if(diffObj.months > 0) s += diffObj.months+" months, "
	s += diffObj.days+" days"
	txt2.SetText( s );
	txt2.SetTextSize( 32 );// may have been undone by autoscale
}

// lazy way to choose month when using Date object 
function setMth(m)
{
	m.jan=0;
	m.feb=1;
	m.mar=2;
	m.apr=3;
	m.may=4;
	m.jun=5;
	m.jul=6;
	m.aug=7;
	m.sep=8;
	m.oct=9;
	m.nov=10;
	m.dec=11;
}
 
// screen orientation has probably changed
function OnConfig()
{
	 if(app.GetOrientation()=="Landscape")
      lay.SetOrientation( "Horizontal" );
  else lay.SetOrientation( "Vertical" );
  //let autoscale do its job again
  txt.SetTextSize( 32 );
  txt2.SetTextSize( 32 );
}
  
// calculate the absolute difference in days, months and years between 2 days
//ignoring any time component
function dateDiff(dt1, dt2)
{
    // setup return object
    var ret = {days:0, months:0, years:0,sign:""};

    // if same date, nothing to do
    if (dt1 == dt2) return ret;

    // we will do absolute diff and set the sign
    if (dt1 > dt2)
    {
        var dtmp = dt2;
        dt2 = dt1;
        dt1 = dtmp;
        ret.sign = "-";
    }
 
    // populate variables for comparison
    var year1 = dt1.getFullYear();
    var year2 = dt2.getFullYear();

    var month1 = dt1.getMonth();
    var month2 = dt2.getMonth();

    var day1 = dt1.getDate();
    var day2 = dt2.getDate();

    // calculate differences
    ret.years = year2 - year1;
    ret.months = month2 - month1;
    ret.days = day2 - day1;
 
    // cope with any negative values.
    if (ret.days < 0)
    {
        // can't span months by arithmetic, use temp date
        var dtmp = new Date(dt1.getFullYear(), dt1.getMonth() + 1, 1, 0, 0, -1);
        var numDays = dtmp.getDate();
 
        ret.months -= 1;
        ret.days += numDays;
    }
 
    // months is pure arithmetic
    if (ret.months < 0)
    {
        ret.months += 12;
        ret.years -= 1;
    }

    return ret;
}